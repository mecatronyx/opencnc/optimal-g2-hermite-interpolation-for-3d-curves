%
%     This file is part of the Optimal G^2 Hermite Interpolation Software.
%
%     Copyright (C) 2017-2019 Raoul Herzog, Philippe Blanc
%                             mecatronYx group at HEIG-VD
%                             University of Applied Sciences Western Switzerland
%                             CH-1401 Yverdon-les-Bains
%                             All rights reserved.
%
%     This is free software; you can redistribute it and/or
%     modify it under the terms of the GNU Lesser General Public
%     License as published by the Free Software Foundation; either
%     version 3 of the License, or (at your option) any later version.
%
%     This software is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%     Lesser General Public License for more details.
%
%     You should have received a copy of the GNU Lesser General Public
%     License along with this software; if not, write to the Free Software
%     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
%
function [r_0D, r_1D, r_2D, r_3D] = polyval3D(p3D, u)
%
P3D_1D = mypolyder(p3D);
P3D_2D = mypolyder(P3D_1D);
P3D_3D = mypolyder(P3D_2D);
%
r_0D = mypolyval(p3D,    u);
r_1D = mypolyval(P3D_1D, u);
r_2D = mypolyval(P3D_2D, u);
r_3D = mypolyval(P3D_3D, u);

