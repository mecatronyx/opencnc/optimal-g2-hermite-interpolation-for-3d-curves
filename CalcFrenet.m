%
%     This file is part of the Optimal G^2 Hermite Interpolation Software.
%
%     Copyright (C) 2017-2019 Raoul Herzog, Philippe Blanc
%                             mecatronYx group at HEIG-VD
%                             University of Applied Sciences Western Switzerland
%                             CH-1401 Yverdon-les-Bains
%                             All rights reserved.
%
%     This is free software; you can redistribute it and/or
%     modify it under the terms of the GNU Lesser General Public
%     License as published by the Free Software Foundation; either
%     version 3 of the License, or (at your option) any later version.
%
%     This software is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%     Lesser General Public License for more details.
%
%     You should have received a copy of the GNU Lesser General Public
%     License along with this software; if not, write to the Free Software
%     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
%
% computes the local Frenet frame (t, n, b) of a curve in R^3
% [t, n, b, kappa] = CalcFrenet(rD1, rD2)
% where rD1 is the first derivative and rD2 the second one
function [t, n, b, kappa] = CalcFrenet(rD1, rD2)
t = rD1 / norm(rD1);          % tangential unit vector
%
if rank([rD1, rD2]) == 2      % regular case
    b     = cross(rD1, rD2);  
    b     = b / norm(b);      % binormal unit vector
    n     = cross(b, t);      % normal unit vector
    kappa = norm(cross(rD1, rD2)) / norm(rD1)^3;  % curvature
else                          % special case if rD2 = 0
    N     = null(rD1');       % construct orthonormal basis of null space
    b     = N(:, 1);          % binormal unit vector
    n     = N(:, 2);          % normal unit vector
    kappa = 0;
end;
    

