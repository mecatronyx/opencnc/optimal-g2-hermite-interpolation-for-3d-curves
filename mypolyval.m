%
%     This file is part of the Optimal G^2 Hermite Interpolation Software.
%
%     Copyright (C) 2017-2019 Raoul Herzog, Philippe Blanc
%                             mecatronYx group at HEIG-VD
%                             University of Applied Sciences Western Switzerland
%                             CH-1401 Yverdon-les-Bains
%                             All rights reserved.
%
%     This is free software; you can redistribute it and/or
%     modify it under the terms of the GNU Lesser General Public
%     License as published by the Free Software Foundation; either
%     version 3 of the License, or (at your option) any later version.
%
%     This software is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%     Lesser General Public License for more details.
%
%     You should have received a copy of the GNU Lesser General Public
%     License along with this software; if not, write to the Free Software
%     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
%
%
function y = mypolyval(p, x)
%POLYVAL Evaluate array of polynomials with same degree.
%
[nD, nc] = size(p);
siz_x    = length(x);
%
% Use Horner's method for general case where X is an array.
y = zeros(nD, siz_x);
if nc > 0, 
    y(:) = repmat(p(:, 1), 1, siz_x); 
end
for i=2:nc
    y = repmat(x, nD, 1) .* y + repmat(p(:, i), 1, siz_x);
end
