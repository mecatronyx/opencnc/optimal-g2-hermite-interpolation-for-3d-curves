%
%     This file is part of the Optimal G^2 Hermite Interpolation Software.
%
%     Copyright (C) 2017-2019 Raoul Herzog, Philippe Blanc
%                             mecatronYx group at HEIG-VD
%                             University of Applied Sciences Western Switzerland
%                             CH-1401 Yverdon-les-Bains
%                             All rights reserved.
%
%     This is free software; you can redistribute it and/or
%     modify it under the terms of the GNU Lesser General Public
%     License as published by the Free Software Foundation; either
%     version 3 of the License, or (at your option) any later version.
%
%     This software is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%     Lesser General Public License for more details.
%
%     You should have received a copy of the GNU Lesser General Public
%     License along with this software; if not, write to the Free Software
%     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
%
clc; clear; close all
%
%% define symbolic parameters (later these parameters will have numerical values)

p0    = sym('p0', [3, 1], 'real');  % initial point
t0    = sym('t0', [3, 1], 'real');  % tangent vector @ initial point
n0    = sym('n0', [3, 1], 'real');  % normal vector  @ initial point
p1    = sym('p1', [3, 1], 'real');  % final point
t1    = sym('t1', [3, 1], 'real');  % tangent vector @ final   point
n1    = sym('n1', [3, 1], 'real');  % normal  vector @ final   point
syms kappa0 kappa1 real             % curvatures at initial and final point
%
%% define 4 symbolic unknowns

syms alpha0 beta0 alpha1 beta1 real
%% define symbolic variable for curve parametrization

syms u real
%
%% define Hermite basis

h00 = (6*u^2+3*u+1)*(1-u)^3;
h10 = (3*u^2+u)*(1-u)^3;
h20 = (1/2)*u^2*(1-u)^3;
h01 = u^3*(6*u^2-15*u+10);
h11 = u^3*(-3*u^2+7*u-4);
h21 = (1/2)*u^3*(u-1)^2;
%% parametric polynomial curve in R^3 with unknown parameters

r_0D(u) = p0*h00 + alpha0*t0*h10 + (beta0*t0 + kappa0*alpha0^2*n0)*h20 + ...
          p1*h01 + alpha1*t1*h11 + (beta1*t1 + kappa1*alpha1^2*n1)*h21;
%% derivatives of parametric curve

r_3D(u) = diff(r_0D(u), u, 3); % 3rd derivative
r_4D(u) = diff(r_0D(u), u, 4); % 4th derivative
%% evaluate integral of square of norm of 3rd derivative

R3D = r_3D(u);
I   = int(R3D(1)^2 + R3D(2)^2 +R3D(3)^2, u, 0, 1); 
matlabFunction(I, 'Vars', {alpha0,beta0,alpha1,beta1, p0, t0, n0, kappa0, p1, t1, n1, kappa1}, 'file','EvalCostIntegral.m');
%% formulate linear equations (19) and (20)

eq1 = r_3D(0)'*t0 == 0;
eq2 = r_3D(1)'*t1 == 0;
LinSys = [eq1; eq2];
%% formulate nonlinear equations (21) and (22)

eq3 = 2*kappa0*alpha0*r_3D(0)'*n0 - r_4D(0)'*t0;
eq4 = 2*kappa1*alpha1*r_3D(1)'*n1 - r_4D(1)'*t1;
%
%% solve linear part (free variables : alpha_0 and alpha_1)

S     = solve(LinSys, [beta0, beta1]); 
%
beta0 = S.beta0;
beta1 = S.beta1;
%
matlabFunction(beta0, beta1, 'Vars', {alpha0, alpha1, p0, t0, n0, kappa0, p1, t1, n1, kappa1}, ...
               'file','Calc_beta0_beta1.m');  
%
eq3_new = simplify(subs(eq3, S));
eq4_new = simplify(subs(eq4, S));
%
L3  = fliplr(coeffs(eq4_new, alpha0));
%
L31 = simplify(fliplr(coeffs(L3(1), alpha1)));
a1  = L31(1);
a0  = L31(2);
L32 = simplify(fliplr(coeffs(L3(2), alpha1)));
b1  = L32(1);
b0  = L32(2);
L33 = simplify(fliplr(coeffs(L3(3), alpha1)));
c3  = L33(1);
c2  = L33(2);
c1  = L33(3);
c0  = L33(4);
%
C3  = fliplr(coeffs(eq3_new, alpha1));
%
C31 = simplify(fliplr(coeffs(C3(1), alpha0)));
d1 = C31(1);
d0 = C31(2);
C32 = simplify(fliplr(coeffs(C3(2), alpha0)));
e1 = C32(1);
e0 = C32(2);
C33 = simplify(fliplr(coeffs(C3(3), alpha0)));
f3  = C33(1);
f2  = C33(2);
f1  = C33(3);
f0  = C33(4);

CoefPS = [a1, a0, b1, b0, c3, c2, c1, c0, d1, d0, e1, e0, f3, f2, f1, f0];
matlabFunction(CoefPS, 'Vars', {p0, t0, n0, kappa0, p1, t1, n1, kappa1}, ...
               'file','CoefPolySys.m');  

%% degenerate case kappa0 = 0
%
clear all; 
syms a1 a0 b1 b0 c3 c2 c1 c0 d1 d0 e1 e0 f3 f2 f1 f0 alpha0 alpha1 real
%
CoefPS = [a1, a0, b1, b0, c3, c2, c1, c0, d1, d0, e1, e0, f3, f2, f1, f0];
%
p1   = (b1*alpha1+b0)*alpha0 + c3*alpha1^3+c2*alpha1^2+c1*alpha1+c0;
p2   = d0*alpha1^2 + e0*alpha1 + f1*alpha0+f0;
%
Fdetalpha0 = feval(symengine, 'polylib::resultant', p1, p2, alpha1);
Coeff_Poly_Alpha0 = fliplr(coeffs(Fdetalpha0, alpha0));
matlabFunction(Coeff_Poly_Alpha0, 'Vars', {CoefPS}, 'file','CharPolyAlpha0.m');  
%
Fdetalpha1 = feval(symengine, 'polylib::resultant', p1, p2, alpha0);
Coeff_Poly_Alpha1 = fliplr(coeffs(Fdetalpha1, alpha1));
matlabFunction(Coeff_Poly_Alpha1, 'Vars', {CoefPS}, 'file','CharPolyAlpha1.m');  
%
%% degenerate case kappa1 = 0
%
clear all; 
syms a1 a0 b1 b0 c3 c2 c1 c0 d1 d0 e1 e0 f3 f2 f1 f0 alpha0 alpha1 real
%
CoefPS = [a1, a0, b1, b0, c3, c2, c1, c0, d1, d0, e1, e0, f3, f2, f1, f0];
%
p1   = a0*alpha0^2 + b0*alpha0 + c1*alpha1+c0;
p2   = (e1*alpha0+e0)*alpha1 + f3*alpha0^3+f2*alpha0^2+f1*alpha0+f0;
%
Fdetalpha0 = feval(symengine, 'polylib::resultant', p1, p2, alpha1);
Coeff_Poly_Alpha0 = fliplr(coeffs(Fdetalpha0, alpha0));
matlabFunction(Coeff_Poly_Alpha0, 'Vars', {CoefPS}, 'file','CharPolyAlpha0.m');  
%
Fdetalpha1 = feval(symengine, 'polylib::resultant', p1, p2, alpha0);
Coeff_Poly_Alpha1 = fliplr(coeffs(Fdetalpha1, alpha1));
matlabFunction(Coeff_Poly_Alpha1, 'Vars', {CoefPS}, 'file','CharPolyAlpha1.m');  
%
%% compute resultant
%
clear all; 
syms a1 a0 b1 b0 c3 c2 c1 c0 d1 d0 e1 e0 f3 f2 f1 f0 alpha1 alpha0 real
CoefPS = sym('CoefPS', 16, 'real');
a1 = CoefPS(1);
a0 = CoefPS(2);
b1 = CoefPS(3);
b0 = CoefPS(4);
c3 = CoefPS(5);
c2 = CoefPS(6);
c1 = CoefPS(7);
c0 = CoefPS(8);
d1 = CoefPS(9);
d0 = CoefPS(10);
e1 = CoefPS(11);
e0 = CoefPS(12);
f3 = CoefPS(13);
f2 = CoefPS(14);
f1 = CoefPS(15);
f0 = CoefPS(16);

p1   = (a1*alpha1+a0)*alpha0^2 + (b1*alpha1+b0)*alpha0 + c3*alpha1^3+c2*alpha1^2+c1*alpha1+c0;
p2   = (d1*alpha0+d0)*alpha1^2 + (e1*alpha0+e0)*alpha1 + f3*alpha0^3+f2*alpha0^2+f1*alpha0+f0;
%
Fdetalpha0 = feval(symengine, 'polylib::resultant', p1, p2, alpha1);
Coeff_Poly_Alpha0 = fliplr(coeffs(Fdetalpha0, alpha0));
matlabFunction(Coeff_Poly_Alpha0, 'Vars', {CoefPS}, 'file','CharPolyAlpha0.m');  
%
Fdetalpha1 = feval(symengine, 'polylib::resultant', p1, p2, alpha0);
Coeff_Poly_Alpha1 = fliplr(coeffs(Fdetalpha1, alpha1));
matlabFunction(Coeff_Poly_Alpha1, 'Vars', {CoefPS}, 'file','CharPolyAlpha1.m');  
%
clear; 
%
% (a1*alpha1+a0)*alpha0^2  +  (b1*alpha1+b0)*alpha0  +  c3*alpha1^3+c2*alpha1^2+c1*alpha1+c0
% (d1*alpha0+d0)*alpha1^2  +  (e1*alpha0+e0)*alpha1  +  f3*alpha0^3+f2*alpha0^2+f1*alpha0+f0
%%
syms a1 a0 b1 b0 c3 c2 c1 c0 d1 d0 e1 e0 f3 f2 f1 f0 alpha1 alpha0 real
CoefPS = [a1, a0, b1, b0, c3, c2, c1, c0, d1, d0, e1, e0, f3, f2, f1, f0];
%% out of the 2nd equation

alpha1_2 = -(1/(d1*alpha0+d0)) * ((e1*alpha0+e0)*alpha1 + f3*alpha0^3+f2*alpha0^2+f1*alpha0+f0);
alpha1_3 = -(1/(d1*alpha0+d0)) * ((e1*alpha0+e0)*alpha1_2 + (f3*alpha0^3+f2*alpha0^2+f1*alpha0+f0)*alpha1);
p1        = (a1*alpha1+a0)*alpha0^2 + (b1*alpha1+b0)*alpha0 + c3*alpha1_3+c2*alpha1_2+c1*alpha1+c0;
alpha1_s = solve(p1==0, alpha1);
matlabFunction(alpha1_s, 'Vars', {alpha0, CoefPS}, 'file','CalcAlpha1.m');
%% out of 1st equation

alpha0_2   = -(1/(a1*alpha1+a0)) * ((b1*alpha1+b0)*alpha0 + c3*alpha1^3+c2*alpha1^2+c1*alpha1+c0);
alpha0_3   = -(1/(a1*alpha1+a0)) * ((b1*alpha1+b0)*alpha0_2 + (c3*alpha1^3+c2*alpha1^2+c1*alpha1+c0)*alpha0);
p2        = (d1*alpha0+d0)*alpha1^2  +  (e1*alpha0+e0)*alpha1  +  f3*alpha0_3+f2*alpha0_2+f1*alpha0+f0;
alpha0_s   = solve(p2==0, alpha0);
matlabFunction(alpha0_s, 'Vars', {alpha1, CoefPS}, 'file','CalcAlpha0.m');