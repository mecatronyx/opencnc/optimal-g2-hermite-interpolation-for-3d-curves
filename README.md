This Matlab software solves a Hermite interpolation problem for a 3D curve where the functional to be minimized is defined as the integral of squared norm of the third parametric derivative, subject to $`G^2`$ continuity constraints at the end points.
The first order necessary optimality condition of the variational problem leads to a parametric transition curve with quintic polynomials.
The determination of coefficients is given by a polynomial system with 2 unknowns.
Stationary points correspond to positive roots of the resultant which is a degree 9 polynomial.

Although the formulated variational problem is non--convex, the proposed approach leads to the global solution,
which can be computed in a reliable and fast manner. 

The example file is executed in Matlab using 
`example_G2_Hermite_Interpolation`

Please cite the following [Open Access Paper](https://www.sciencedirect.com/science/article/pii/S0010448518304305) if you are using our software :

```latex
@Article{Herzog_Blanc2019,
Author = {Raoul Herzog and Philippe Blanc},
Title = {Optimal G^2 Hermite Interpolation for 3D Curves},
Journal = {Elsevier Computer--Aided Design},
Year = {2019},
Month = 12,
Volume  = {117},
doi = {10.1016/j.cad.2019.102752},
url = {https://www.sciencedirect.com/science/article/pii/S0010448518304305}
}

    
    