%
%     This file is part of the Optimal G^2 Hermite Interpolation Software.
%
%     Copyright (C) 2017-2019 Raoul Herzog, Philippe Blanc
%                             mecatronYx group at HEIG-VD
%                             University of Applied Sciences Western Switzerland
%                             CH-1401 Yverdon-les-Bains
%                             All rights reserved.
%
%     This is free software; you can redistribute it and/or
%     modify it under the terms of the GNU Lesser General Public
%     License as published by the Free Software Foundation; either
%     version 3 of the License, or (at your option) any later version.
%
%     This software is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%     Lesser General Public License for more details.
%
%     You should have received a copy of the GNU Lesser General Public
%     License along with this software; if not, write to the Free Software
%     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
%
clc; clear; close all;
%
% Example for "Optimal G^2 Hermite Interpolation"
% creates figure 1 of the paper "Optimal G^2 Hermite interpolation for 3D
% curves", to appear in Computer-Aided Design, Elsevier 
%
% 
r0D0 = [1;  0;  0];  % coordinates of point p_0
r1D0 = [0;  1;  0];  % coordinates of point p_1
r0D1 = [0;  5;  0];  % 1st parametric derivative at p_0
r1D1 = [-5; 0;  0];  % 1st parametric derivative at p_1
r0D2 = [0;  0;  0];  % 2nd parametric derivative at p_0
r1D2 = [0;  0;  0];  % 2nd parametric derivative at p_1
%
p5 = G2_Hermite_Interpolation(r0D0, r0D1, r0D2, r1D0, r1D1, r1D2);
%
u = linspace(0, 1, 1000);
[r_0D, r_1D, r_2D, r_3D] = polyval3D(p5, u); 
%
figure
plot(r_0D(1, :), r_0D(2, :), [1, 1, 0], [0, 1, 1], 'r--');
axis('equal');
grid;
title('optimal G^2 Hermite interpolant');
xlabel('x');
ylabel('y');
ylim([-0.05, 1.05]);


